import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

public class Neurons extends JFrame implements ActionListener, Runnable {
    public static void main(String args[]) { (new Neurons()).show(true); }

    /**
     * Back Propagation
     */
    public static final int METHOD_BACKPROP = 1;

    /**
     * Simulated Annealing
     */
    public static final int METHOD_ANNEAL = 2;

    /**
     * Genetic Algorithm
     */
    public static final int METHOD_GENETIC = 3;

    /**
     * Training mode
     */
    protected int mode = METHOD_BACKPROP;

    /**
     * The background worker thread.
     */
    protected Thread worker = null;

    /**
     * The number of input neurons.
     */
    protected final static int NUM_INPUT = 2;

    /**
     * The number of output neurons.
     */
    protected final static int NUM_OUTPUT = 1;

    /**
     * The number of hidden neurons.
     */
    protected final static int NUM_HIDDEN = 3;

    /**
     * The learning rate.
     */
    protected final static double RATE = 0.5;

    /**
     * The learning momentum.
     */
    protected final static double MOMENTUM = 0.7;

    /**
     *     Buttons
     */
    JButton anneal;
    JButton runme;

    /**
     * The neural network.
     */
    protected Network network;


    /**
     * Constructor. Setup the components.
     */
    public Neurons() {
        setTitle("Neurons");
        network = new Network(
                NUM_INPUT,
                NUM_HIDDEN,
                NUM_OUTPUT,
                RATE,
                MOMENTUM);

        Container content = getContentPane();

        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();
        content.setLayout(gridbag);

        c.fill = GridBagConstraints.NONE;
        c.weightx = 1.0;

        // Training input label
        c.gridwidth = GridBagConstraints.REMAINDER; //end row
        c.anchor = GridBagConstraints.NORTHWEST;

        // the button panel
        JPanel buttonPanel = new JPanel(new FlowLayout());


        // Add the button panel
        c.gridwidth = GridBagConstraints.REMAINDER; //end row
        c.anchor = GridBagConstraints.CENTER;
        content.add(buttonPanel,c);

        // Training input label
        c.gridwidth = GridBagConstraints.REMAINDER; //end row
        c.anchor = GridBagConstraints.NORTHWEST;

        // adjust size and position
        pack();
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension d = toolkit.getScreenSize();
        setLocation(
                (int)(d.width-this.getSize().getWidth())/2,
                (int)(d.height-this.getSize().getHeight())/2);
        setResizable(true);
        setSize(700,100);

        buttonPanel.add(runme = new JButton());
        runme.setText("Click Here To Let Your Computer Run Itself");
        runme.addActionListener(this);
        runme.show();

        buttonPanel.add(anneal = new JButton());
        anneal.setText("Click Here To Train Your Assistant");
        anneal.addActionListener(this);
        anneal.show();

    }
    /**
     * Called when the user clicks one of the three
     * buttons.
     *
     * @param e The event.
     */
    int stopper = 1;
    public void actionPerformed(ActionEvent e)
    {
        if ( e.getSource()==anneal ) {
            begin(METHOD_BACKPROP);
            stopper = 50;
        }
        else if ( e.getSource()==runme )
            try {
                evaluate();
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
    }

    public void evaluate() throws InterruptedException, IOException {
        parse1 clear = new parse1();
        parse2 weed = new parse2();
        parse3 set = new parse3();
        parse4 learn = new parse4();
        Feed feeder = new Feed();
        while(stopper < 5) {
            feeder.feeding();
            clear.Clearing();
            weed.Weeding();
            set.Setting();
            learn.Learning();
            Thread.sleep(10000);
        }
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        } catch(NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

    protected void begin(int mode)
    {
        this.mode = mode;

        if ( worker != null )
            worker = null;
        worker = new Thread(this);
        worker.setPriority(Thread.MIN_PRIORITY);
        worker.start();
    }

    public void run() {

    }
}