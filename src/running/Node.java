import java.util.Arrays;
import java.util.Collections;

public class Node {
    int cValue;
    String clock;
    String name;
    boolean running;

    public Node(String name, String clock, int cValue) {
        this.name = name;
        this.clock = clock;
        this.cValue = cValue;
    }

    public String getInfo() {
        return this.name + " " + this.clock + " " + this.cValue;
    }
}

class NaiveBayesModel {
	Classifier<String, String> bayes;

    NaiveBayesModel() {
        bayes = new BayesClassifier<>();
    }

	void learn(String catagory, String[] data) {
		bayes.learn(catagory, Arrays.asList(data));
	}
	
	String classify(String unClassified) {
		 return bayes.classify(Collections.singletonList(unClassified)).getCategory();
	}
}
