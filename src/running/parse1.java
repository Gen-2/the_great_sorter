import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class parse1 {
    public void Clearing() {
        try {
            //creating File instance to reference text file in Java
            File text = new File("output.txt");
            Scanner scnr = new Scanner(text);
            RandomAccessFile texts = new RandomAccessFile(text, "rw");

            File oldFile = new File("olds.txt");       // Your temp
            BufferedWriter deleter = new BufferedWriter(new FileWriter(oldFile));

            int lineNumber = 0;
            List<String> fileHolder = new ArrayList<>();
            String line = null;

            while (scnr.hasNextLine()) {
                line = scnr.nextLine();
                fileHolder.add(lineNumber,line);

                if ( (line.contains("Window Title: ")) ) {
                    if (line.contains("N/A")){
                        //System.out.println("boo");
                        while(lineNumber != 0){
                            fileHolder.remove(lineNumber);
                            lineNumber--;
                        }
                        fileHolder.remove(0);
                        lineNumber = 0;
                    }
                    else {
                        int i = 0;
                        String s = null;
                        try {
                            do{
                                deleter.write(fileHolder.get(i));
                                deleter.newLine();
                                // System.out.println(fileHolder.get(i));
                                i++;
                            } while (fileHolder.size() >= i);
                        }
                        catch(IndexOutOfBoundsException ex) {
                            while(lineNumber != 0){
                                fileHolder.remove(lineNumber);
                                lineNumber--;
                            }
                            fileHolder.remove(0);
                            lineNumber = 0;
                        }
                    }
                }
                else {
                    // System.out.println("YAY");
                    // System.out.println(lineNumber);
                    if(!line.contains("\n"))
                        lineNumber++;
                }
            }
            texts.close();
            deleter.close();
        }
        catch (FileNotFoundException a){
            System.exit(0);
            JOptionPane box = new JOptionPane();
            box.setMessage("Sorry about this, the file you are looking for is not available. Please re-install the application, thank you.");
            box.show();
        }
        catch (IOException e) {
            System.exit(0);
            JOptionPane box = new JOptionPane();
            box.setMessage("Sorry about this, the file you are looking for is not available. Please re-install the application, thank you.");
            box.show();
        }
    }
}
