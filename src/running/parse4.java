import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Scanner;

public class parse4 {

    private int mem;
    private int cpu;
    private String image;
    private LocalTime time;
    public void Learning() {
        try {
            int a = 0;
            File INPUT = new File("INPUT.TXT");
            Scanner reads = new Scanner(INPUT);

            NaiveBayesModel bayes = new NaiveBayesModel();
            String scanned;
            File dir = new File("C:\\Program Files\\");
            File[] compil = dir.listFiles();
            System.out.println(Arrays.toString(compil));
            Node[] newNode = new Node[1000];


            File usefulTrainingData = new File("Useful.trn");
            File uselessTrainingData = new File("Useless.trn");
            Scanner useful = new Scanner(usefulTrainingData);
            Scanner useless = new Scanner(uselessTrainingData);
            String[] usefulInput = new String[useful.nextInt()];
            String[] uselessInput = new String[useless.nextInt()];

            populateTrainingArray(useful, usefulInput);
            populateTrainingArray(useless, uselessInput);

            bayes.learn("useful", usefulInput);
            bayes.learn("useless", uselessInput);

            do {

                time = LocalTime.now();

                String chron;
                String[] chronArray;
                chron = String.format("%s", time);
                chronArray = chron.split("[: .]+");
                chron = chronArray[0] + chronArray[1] + chronArray[2];

                int cValue = mem + cpu;

                Node nodius = new Node(image, chron, cValue);
                nodius.running = true;
                newNode[a] = nodius;

                int matchNumber = 0;
                System.out.println("<~~~~~~~~~~~~~~~~~~~~~~~~~~~~+++++++++++A: " + a);

                for (Node aNewNode : newNode) {
                    if (aNewNode != null) {
                        System.out.println(aNewNode.getInfo());
                        if (Objects.equals(aNewNode.name, newNode[a].name)) {
                            matchNumber = matchNumber + 1;
                            System.out.println("<~~~~~~~~~~~~~~~~~+++++++++++MATCH:" + matchNumber);
                        }
                    }
                }

                System.out.println(bayes.classify(nodius.getInfo()));

                scanned = reads.nextLine();
                image = scanned;
                scanned = reads.nextLine();
                mem = Integer.parseInt(scanned);
                scanned = reads.nextLine();
                cpu = Integer.parseInt(scanned);

                a++;
                if ( !reads.hasNextLine() ) break;
            } while ( reads.hasNextLine() );

        } catch (FileNotFoundException e) {
            JOptionPane box = new JOptionPane();
            box.setMessage("Something just went seriously wrong. Please re-install this application. Thank you.");
            box.show();
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

    public void populateTrainingArray (Scanner scans, String[] strings) {
        for(int a = 0; scans.hasNextLine(); a ++){
            strings[a] = scans.nextLine();
            System.out.println(strings[a]);
        }
    }
}
