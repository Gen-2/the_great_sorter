import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Feed {

    File usefullFile = new File("Useful.trn");
    File uselessFile = new File("Useless.trn");

    public Feed () { }

    public void feeding() throws IOException {

        //FIX HERE: FIND AN ALGORITHM TO ASSESS USEFULNESS AS A FUNCTION OF LOW NUMBERS, BAD NAMES, AND UNIQUENESS
        //FIX HERE: FIND AN ALGORITHM TO ASSESS USEFULNESS AS A FUNCTION OF HIGH NUMBERS, GOOD NAMES, AND COMMONALITY

        BufferedWriter writerOfUse = new BufferedWriter(new FileWriter(usefullFile));
        BufferedWriter writerOfNoUse = new BufferedWriter(new FileWriter(uselessFile));

        writerOfNoUse.write("10\n" +
                "thunderbird.exe 141715 42532\n" +
                "discord.exe 141715 41335\n" +
                "Steam.exe 141715 28026\n" +
                "firefox.exe 141715 341792\n" +
                "firefox.exe 141715 223086\n" +
                "cinema4d.exe 141715 1340\n" +
                "WinStore.App.exe 141715 1315\n" +
                "Steam.exe 141715 34257\n" +
                "GOOSE GOOSE DUCK");
        writerOfUse.write("10\n" +
                "LockApp.exe 1 1\n" +
                "smartscreen.exe 1 3\n" +
                "RtkNGUI64.exe 4 2\n" +
                "TabletDriver.exe 141 12\n" +
                "avguix.exe 14 19\n" +
                "boinctray.exe 16 59\n" +
                "javaw.exe 140 64\n" +
                "RuntimeBroker.exe 34 62\n" +
                "DUCK DUCK GOOSE");

        writerOfNoUse.close();
        writerOfUse.close();
    }
}
