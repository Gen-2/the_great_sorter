import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class parse2 {
    public void Weeding() {
        try {
            File oldFile = new File("olds.txt");
            File ArrayOld = new File("ArrayOld.txt");

            RandomAccessFile older = new RandomAccessFile(oldFile, "rw");
            RandomAccessFile arrays = new RandomAccessFile(ArrayOld, "rw");

            BufferedWriter oldies = new BufferedWriter(new FileWriter(ArrayOld));

            List<String> exe = new ArrayList<>();
            List<String> memory = new ArrayList<>();
            List<String> cpu = new ArrayList<>();

            Scanner reads = new Scanner(oldFile);
            String lines = null;
            do {
                lines = reads.nextLine();
                if (lines.contains("Image Name:")) {
                    //System.out.println(lines);
                    oldies.write(lines);
                    oldies.newLine();
                } else if (lines.contains("Mem Usage:")) {
                    //System.out.println(lines);
                    oldies.write(lines);
                    oldies.newLine();
                } else if (lines.contains("CPU Time:")) {
                    //System.out.println(lines);
                    oldies.write(lines);
                    oldies.newLine();
                    oldies.newLine();
                }
            } while(reads.hasNextLine());

            arrays.close();
            older.close();
            oldies.close();

        } catch (FileNotFoundException i) {
            JOptionPane box = new JOptionPane();
            box.setMessage("Something just went seriously wrong. Please re-install this application. Thank you.");
            box.show();
        } catch (IOException e) {
            JOptionPane box = new JOptionPane();
            box.setMessage("Something just went seriously wrong. Please re-install this application. Thank you.");
            box.show();
        }
    }

}
