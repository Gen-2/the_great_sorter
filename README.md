<body>
<center>
<h1>The Great Sorter</h1>
<h3>Certified with: "It works on my machine!"</h3>
</center>
<left>
<h2>What is it?</h2>
<p>An AI project developed by the one and only Gen-2</p>

<h2>What does it do?</h2>
<p>It sorts through all of the open apps on your computer and then, through the use of the Naive Bayes algorithm and other machine learning techniques, opens the apps you usually open at the times you usually open them.</p>

<h2>When will it be done?</h2>
<p>Soon my friend, soon enough! In all reality it has been in production about two and a half years and seems to be going to take about another year.</p>

<h2>Tips</h2>

<p>1) Delete App/running/*.txt. All four of those files are filled with input from my own machine. They will only make you computers data muddied.</p>
<p>2) This app was made solely in IntelliJ idea, I wish anyone using any other IDE the best of luck.</p>
<p>3) Don't bother making some crazy Maven or Gradle config to make this work. Run it useing App/running/runner.exe. It will do all that is needed to help the app run correctly.</p>
<p>4) If for some reason you would like to change the input of this app, feel free to modify runner.cpp. It is the source for runner.exe.</p>
<p>5) This app is written with a monolithic structure in mind first. It was broken up afterward. Keep this in mind when debugging.</p>

</left>
</body>